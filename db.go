package main

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"strconv"
	"strings"
)

func PgConnect(host string, port int, user string, pass string, dbname string) (conn *sql.DB, err error) {
	if user == "" {
		return nil, errors.New("User for database is not specified")
	}

	if pass == "" {
		return nil, errors.New("Pass for host database is not specified")
	}

	var params []string
	if host != "" {
		params = append(params, "host="+host)
	}
	if port != 0 {
		params = append(params, "port="+strconv.Itoa(port))
	}
	params = append(params, "user="+user)
	params = append(params, "password="+pass)
	if dbname != "" {
		params = append(params, "dbname="+dbname)
	}
	params = append(params, "sslmode=disable")

	connstr := strings.Join(params[:], " ")
	conn, err = sql.Open("postgres", connstr)

	if err = CheckConnection(conn); err != nil {
		return nil, err
	}

	return conn, nil
}

func CheckConnection(db *sql.DB) error {
	err := db.Ping()
	return err
}

func IsFixableTextType(tp string) bool {
	switch tp {
	case "character":
		return true
	}
	return false
}

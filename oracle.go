package main

import (
	"database/sql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	// _ "gopkg.in/rana/ora.v4"
	_ "github.com/mattn/go-oci8"
	"regexp"
	"strings"
)

type OracleAdapter struct {
	db *sql.DB
}

func (adapter *OracleAdapter) GetType() string {
	return ORACLE
}

func (adapter *OracleAdapter) Connect(database *Database) (err error) {
	dsn := fmt.Sprintf("%s/%s@%s:%d/%s?prefetch_rows=100000", database.User, database.Pass, database.Host, database.Port, database.Name)

	if err != nil {
		return err
	}

	if adapter.db, err = sql.Open("oci8", dsn); err != nil {
		return err
	}

	err = adapter.db.Ping()
	return err
}

func (adapter *OracleAdapter) GetTable(schemaName string, table *Table) (err error) {
	rows, err := adapter.db.Query(`
	SELECT
		COLUMN_NAME, DATA_TYPE, DATA_LENGTH
	FROM
		ALL_TAB_COLUMNS
	WHERE
		OWNER = UPPER(:1)
		AND TABLE_NAME = UPPER(:2)`, table.Schema.Name, table.Name)
	if err != nil {
		return err
	}
	defer rows.Close()

	type Column struct {
		Name   string
		Type   string
		Length int
	}

	for rows.Next() {
		var col Column
		if err = rows.Scan(&col.Name, &col.Type, &col.Length); err != nil {
			return err
		}

		var field Field
		field.Name = strings.ToLower(col.Name)

		rgx := regexp.MustCompile(`(.*)\((.*)\)`)
		rs := rgx.FindStringSubmatch(col.Type)

		field.Type = col.Type
		if len(rs) == 3 {
			field.Type = rs[1]
		}

		field.Type = GetPostgresFromOracleType(field.Type)
		if field.Type == "character varying" {
			field.MaxLen = new(int)
			*field.MaxLen = col.Length
		}
		table.Fields = append(table.Fields, field)
	}

	return nil
}

func (adapter *OracleAdapter) DownloadData(table Table) <-chan []interface{} {
	ch := make(chan []interface{})
	go func() {
		defer close(ch)

		columnNames := make([]string, len(table.Fields))
		for i, field := range table.Fields {
			columnNames[i] = field.Name
		}
		query := fmt.Sprintf("SELECT %s FROM %s.%s", strings.Join(columnNames, ", "), table.Schema.Name, table.Name)
		if table.Where != "" {
			query += " WHERE " + table.Where
		}
		if table.Order != "" {
			query += " ORDER BY " + table.Order
		}
		if table.Limit != 0 {
			query = fmt.Sprintf("SELECT * FROM (%s) WHERE ROWNUM <= %d", query, table.Limit)
		}
		table.Logger.WithFields(log.Fields{
			"query": query,
		}).Debug("Load data")

		rows, err := adapter.db.Query(query)
		if err != nil {
			table.Logger.WithFields(log.Fields{
				"err": err,
			}).Error("Cannot load data")
			return
		}
		defer rows.Close()

		for rows.Next() {
			values := make([]interface{}, len(table.Fields))
			valuePtrs := make([]interface{}, len(table.Fields))
			for i := 0; i < len(table.Fields); i++ {
				valuePtrs[i] = &values[i]
			}

			if err := rows.Scan(valuePtrs...); err != nil {
				log.WithFields(log.Fields{
					"err": err,
				}).Error("ERROR")
				continue
			}

			ch <- values
		}

	}()
	return ch
}

func GetPostgresFromOracleType(tp string) string {
	switch strings.ToLower(tp) {
	case "number":
		return "integer"
	case "varchar2":
		return "character varying"
	case "nvarchar2":
		return "character varying"
	case "date":
		return "timestamp without time zone"
	}
	return strings.ToLower(tp)
}

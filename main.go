package main

import (
	log "github.com/Sirupsen/logrus"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	config = kingpin.Arg("config", "YAML config file").Required().String()
)

func main() {
	kingpin.Version("0.0.1")
	kingpin.Parse()

	if *config == "" {
		kingpin.FatalUsage("Config was not specified")
	}

	InitLogger()
	log.Debug("Logger started")

	var conf Config
	if err := ParseConfig(*config, &conf); err != nil {
		log.Fatal("Cannot parse config")
	}

	db, err := PgConnect(conf.Host, conf.Port, conf.User, conf.Pass, "postgres")
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Error("Cannot connect to destination database")
		panic(err)
	}
	defer db.Close()
	LoadDatabases(conf, db)
}

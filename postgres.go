package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"strings"
)

type PostgresAdapter struct {
	db *sql.DB
}

func (adapter *PostgresAdapter) GetType() string {
	return POSTGRES
}

func (adapter *PostgresAdapter) Connect(database *Database) (err error) {
	adapter.db, err = PgConnect(database.Host, database.Port, database.User, database.Pass, database.Name)
	return err
}

func (adapter *PostgresAdapter) GetTable(schemaName string, table *Table) (err error) {
	query := `
	SELECT
	column_name, data_type, character_maximum_length
	FROM
	information_schema.columns
	WHERE
	table_schema = $1
	AND table_name = $2
	;`

	rows, err := adapter.db.Query(query, schemaName, table.Name)
	defer rows.Close()

	for rows.Next() {
		var field Field

		if err = rows.Scan(&field.Name, &field.Type, &field.MaxLen); err != nil {
			return err
		}

		// TODO: better support, use udt_name
		if field.Type == "USER-DEFINED" {
			field.Type = "text"
		}
		table.Fields = append(table.Fields, field)
	}

	if len(table.Fields) == 0 {
		return fmt.Errorf("No fields")
	}

	pkquery := `
	SELECT
	c.column_name
	FROM
	information_schema.key_column_usage AS c
	LEFT JOIN
	information_schema.table_constraints AS t ON t.constraint_name = c.constraint_name
	WHERE
	t.table_schema = $1
	AND t.table_name = $2
	AND t.constraint_type = 'PRIMARY KEY';
	;`
	pkrows, err := adapter.db.Query(pkquery, schemaName, table.Name)
	defer pkrows.Close()
	for pkrows.Next() {
		var fieldName string
		if err = pkrows.Scan(&fieldName); err != nil {
			return err
		}

		for i := range table.Fields {
			field := &table.Fields[i]
			if field.Name == fieldName {
				field.PrimaryKey = true
			}
		}
	}

	return nil
}

func (adapter *PostgresAdapter) DownloadData(table Table) <-chan []interface{} {
	ch := make(chan []interface{})
	go func() {
		defer close(ch)

		columnNames := make([]string, len(table.Fields))
		for i, field := range table.Fields {
			columnNames[i] = field.Name
		}
		query := fmt.Sprintf("SELECT %s FROM %s.%s", strings.Join(columnNames, ", "), table.Schema.Name, table.Name)
		if table.Where != "" {
			query += " WHERE " + table.Where
		}
		if table.Order != "" {
			query += " ORDER BY " + table.Order
		}
		if table.Limit != 0 {
			query += fmt.Sprintf(" LIMIT %d", table.Limit)
		}
		rows, err := adapter.db.Query(query)
		if err != nil {
			return
		}
		defer rows.Close()

		for rows.Next() {
			values := make([]interface{}, len(table.Fields))
			valuePtrs := make([]interface{}, len(table.Fields))

			for i := range table.Fields {
				valuePtrs[i] = &values[i]
			}
			if err := rows.Scan(valuePtrs...); err != nil {
				continue
			}

			ch <- values
		}

	}()
	return ch
}

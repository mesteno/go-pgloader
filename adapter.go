package main

import (
	"fmt"
)

type DatabaseAdapter interface {
	GetType() string
	Connect(database *Database) error
	GetTable(schemaName string, table *Table) error
	DownloadData(table Table) <-chan []interface{}
	// TODO: add table existance check
}

const (
	POSTGRES = "postgres"
	ORACLE   = "oracle"
	MSSQL    = "mssql"
)

type AdapterFactory struct{}

func (factory *AdapterFactory) Create(dbtype string) (DatabaseAdapter, error) {
	switch dbtype {
	case POSTGRES:
		return new(PostgresAdapter), nil
	case ORACLE:
		return new(OracleAdapter), nil
	case MSSQL:
		return new(MssqlAdapter), nil
	default:
		return nil, fmt.Errorf("Unknown database type: %s\n", dbtype)
	}
}

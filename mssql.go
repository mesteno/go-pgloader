package main

import (
	"database/sql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	_ "github.com/denisenkom/go-mssqldb"
	"net/url"
	"strings"
)

type MssqlAdapter struct {
	db *sql.DB
}

func (adapter *MssqlAdapter) GetType() string {
	return MSSQL
}

func (adapter *MssqlAdapter) Connect(database *Database) (err error) {
	query := url.Values{}
	query.Add("connection timeout", "600")

	u := &url.URL{
		Scheme:   "sqlserver",
		User:     url.UserPassword(database.User, database.Pass),
		Host:     database.Host,
		RawQuery: query.Encode(),
	}

	connstr := u.String()
	log.WithFields(log.Fields{
		"connstr":  connstr,
		"database": database.Name,
	}).Debug("Connecting to database")

	if err != nil {
		return err
	}

	if adapter.db, err = sql.Open("mssql", connstr); err != nil {
		return err
	}

	err = adapter.db.Ping()
	return err
}

func (adapter *MssqlAdapter) GetTable(schemaName string, table *Table) (err error) {
	// TODO: FIX WITHOUT SCHEMA
	query := `
	SELECT
		COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH
	FROM
		INFORMATION_SCHEMA.COLUMNS`

	var where_fields []string
	var query_params []interface{}
	where_fields = append(where_fields, "TABLE_NAME = ?1")
	query_params = append(query_params, table.Name)

	if table.Schema.Name != "" {
		where_fields = append(where_fields, "TABLE_SCHEMA = ?2")
		query_params = append(query_params, table.Schema.Name)
	}

	if len(where_fields) > 0 {
		query += " WHERE " + strings.Join(where_fields, " AND ")
	}

	rows, err := adapter.db.Query(query, query_params...)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var field Field
		var maxLen sql.NullInt64

		if err = rows.Scan(&field.Name, &field.Type, &maxLen); err != nil {
			return err
		}

		field.Name = strings.ToLower(field.Name)

		field.Type = GetPostgresFromMssqlType(field.Type)
		if field.Type == "character varying" && maxLen.Valid && maxLen.Int64 > 0 {
			field.MaxLen = new(int)
			*field.MaxLen = int(maxLen.Int64)
		}
		table.Fields = append(table.Fields, field)
	}

	return nil
}

func (adapter *MssqlAdapter) DownloadData(table Table) <-chan []interface{} {
	ch := make(chan []interface{})
	go func() {
		defer close(ch)

		columnNames := make([]string, len(table.Fields))
		for i, field := range table.Fields {
			columnNames[i] = "\"" + field.Name + "\""
		}

		var query string
		if table.Limit != 0 {
			query = fmt.Sprintf("SELECT TOP %d ", table.Limit)
		} else {
			query = "SELECT "
		}

		var fullTableName = table.Name
		if table.Schema.Name != "" {
			fullTableName = table.Schema.Name + "." + table.Name
		}

		// TODO: FIX
		query += fmt.Sprintf("%s FROM %s", strings.Join(columnNames, ", "), fullTableName)
		if table.Where != "" {
			query += " WHERE " + table.Where
		}
		if table.Order != "" {
			query += " ORDER BY " + table.Order
		}
		table.Logger.WithFields(log.Fields{
			"query": query,
		}).Debug("Load data")

		rows, err := adapter.db.Query(query)
		if err != nil {
			table.Logger.WithFields(log.Fields{
				"err": err,
			}).Error("Cannot load data")
			return
		}
		defer rows.Close()

		for rows.Next() {
			values := make([]interface{}, len(table.Fields))
			valuePtrs := make([]interface{}, len(table.Fields))

			for i := range table.Fields {
				valuePtrs[i] = &values[i]
			}
			if err := rows.Scan(valuePtrs...); err != nil {
				continue
			}

			ch <- values
		}

	}()
	return ch
}

func GetPostgresFromMssqlType(tp string) string {
	switch strings.ToLower(tp) {
	case "int":
		return "integer"
	case "tinyint":
		return "smallint"
	case "nvarchar":
		return "character varying"
	case "varchar":
		return "character varying"
	case "smalldatetime":
		return "timestamp without time zone"
	case "datetime":
		return "timestamp without time zone"
	case "bit":
		return "boolean"
	}
	return strings.ToLower(tp)
}

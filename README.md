# go-pgloader

## Описание
ПО предназначено для быстрой загрузки данных из одной базы в другую.
Поддерживаемые типы баз данных источников:

1. PostgreSQL
2. Oracle SQL
3. MSSQL

Поддерживаемые типы баз данных преемников:

1. PostgreSQL

## Установка с помощью Docker
```
docker build -t pgloader .
```

## Запуск

Для запуска необходимо указать путь к файлу в формате YAML с описанием загружаемых таблиц и необходимым доступами

```
go-pgloader config.yaml
```

## Конфигурация

Конфигурационный файл должен быть записан в формате YAML.
Пример конфигурационного файла:
```
databases:
  - name: test
    type: postgres
    host: localhost
    port: 5432
    user: test
    pass: test
    drop: true
    schemas:
      - name: public
        tables:
          - name: test
            where: "id < 1000"
            limit: 100
            order: "id DESC"
host: localhost
port: 5432
user: test
pass: test
prefix: pg_
```

- databases[] : Описывает список баз данных источников
- databases[].name : Название базы источника (или SERVICE_NAME для ORACLE)
- databases[].type : Тип базы данных источника (поддерживаемые значения: posgres, oracle, mssql)
- databases[].host : Адрес сервера базы данных источника
- databases[].port : Порт базы данных источника
- databases[].user : Имя пользователя базы данных источника
- databases[].pass : Пароль пользователя базы данных источника
- databases[].drop : Удалять ли базу данных на стороне преемника (true | [false])
- databases[].schemas : Список загружаемых схем
- databases[].schemas[].name : Название схемы
- databases[].schemas[].tables : Список загружаемых таблиц
- databases[].schemas[].tables[].name : Название таблицы
- databases[].schemas[].tables[].where : Фильтр данных (опциональный параметр)
- databases[].schemas[].tables[].limit: Ограничение количества строк (опциональный параметр)
- databases[].schemas[].tables[].order: Порядок строк при загрузке (опциональный параметр)
- host : Сервер базы данных преемника
- port : Порт базы данных преемника
- user : Логин для базы преемника
- pass : Пароль пользователя базы преемника
- prefix : Префикс для создаваемой базы данных

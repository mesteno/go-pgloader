package main

import (
	log "github.com/Sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Config struct {
	Databases []Database
	Prefix    string
	Host      string
	Port      int
	User      string
	Pass      string
}

type Database struct {
	Name    string
	Type    string
	Host    string
	Port    int
	User    string
	Pass    string
	Drop    bool
	Schemas []Schema
}

type Schema struct {
	Name   string
	Tables []Table
}

type Table struct {
	Name  string
	Where string
	Order string
	Limit int

	Schema *Schema
	Fields []Field
	Logger *log.Entry
}

type Field struct {
	Name       string
	Type       string
	MaxLen     *int
	PrimaryKey bool
}

func ParseConfig(fileName string, conf *Config) error {
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}

	if err := yaml.Unmarshal(data, conf); err != nil {
		return err
	}

	return nil
}

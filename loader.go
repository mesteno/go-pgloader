package main

import (
	"database/sql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/lib/pq"
	"strconv"
	"strings"
)

func LoadDatabases(config Config, db *sql.DB) {
	var af AdapterFactory
	for _, database := range config.Databases {
		adapter, err := af.Create(database.Type)
		if err != nil {
			log.WithFields(log.Fields{
				"database": database.Name,
				"err":      err,
			}).Error("Cannot create database")
			continue
		}

		if err := LoadDatabase(db, adapter, &database, &config); err != nil {
			log.WithFields(log.Fields{
				"database": database.Name,
				"err":      err,
			}).Error("Cannot load database")
		}
	}
}

func LoadDatabase(destDb *sql.DB, adapter DatabaseAdapter, database *Database, config *Config) error {
	if err := adapter.Connect(database); err != nil {
		log.WithFields(log.Fields{
			"database": database.Name,
			"err":      err,
		}).Error("Connection failed")
		return fmt.Errorf("Cannot connect to database")
	}
	log.WithFields(log.Fields{
		"database": database.Name,
	}).Info("Connection to database established")

	conn, err := CreateDatabase(destDb, database, config)
	if err != nil {
		return err
	}
	defer conn.Close()

	for _, schema := range database.Schemas {
		schema.Name = strings.ToLower(schema.Name)

		// TODO: FIX
		if schema.Name == "" {
			schema.Name = "public"
		}

		if err := CreateSchema(conn, schema.Name); err != nil {
			log.WithFields(log.Fields{
				"schema": schema.Name,
			}).Error("Cannot create schema")
			continue
		}

		for _, table := range schema.Tables {
			table.Schema = &schema
			table.Name = strings.ToLower(table.Name)
			table.Logger = log.WithFields(log.Fields{
				"database": database.Name,
				"schema":   schema.Name,
				"table":    table.Name,
			})

			err := adapter.GetTable(schema.Name, &table)
			if err != nil {
				table.Logger.WithFields(log.Fields{
					"err": err,
				}).Error("Cannot get table info")
				continue
			}

			if err := CreateTable(conn, table); err != nil {
				table.Logger.WithFields(log.Fields{
					"err": err,
				}).Error("Cannot create table")
				continue
			}

			if err := LoadTable(conn, adapter, table); err != nil {
				table.Logger.WithFields(log.Fields{
					"err": err,
				}).Error("Cannot load data")
				continue
			}

			if err := CreatePrimaryKey(conn, table); err != nil {
				table.Logger.WithFields(log.Fields{
					"table": table.Name,
					"err":   err,
				}).Error("Cannot create primary key")
				continue
			}
		}
	}

	return nil
}

func CreateDatabase(db *sql.DB, database *Database, config *Config) (conn *sql.DB, err error) {
	dbname := config.Prefix + database.Name
	dblog := log.WithFields(log.Fields{
		"database": dbname,
	})

	if IsDatabaseExists(db, dbname) && database.Drop {
		dblog.Info("Database already exists, dropping")

		if _, err := db.Exec("DROP DATABASE \"" + dbname + "\""); err != nil {
			dblog.Error("Cannot drop database")
		}
	}

	dblog.Info("Creating database")

	if !IsDatabaseExists(db, dbname) {
		if _, err := db.Exec("CREATE DATABASE \"" + dbname + "\""); err != nil {
			return nil, err
		}
	}

	conn, err = PgConnect(config.Host, config.Port, config.User, config.Pass, dbname)
	if err != nil {
		return nil, err
	}

	dblog.Info("Database created successfully")
	return conn, nil
}

func IsDatabaseExists(db *sql.DB, dbname string) bool {
	var exists int
	if err := db.QueryRow(`SELECT 1 FROM pg_database WHERE datname=$1`, dbname).Scan(&exists); err != nil {
		return false
	}

	return exists == 1
}

func CreateSchema(db *sql.DB, schema string) error {
	log.WithFields(log.Fields{
		"schema": schema,
	}).Info("Creting schema")

	if _, err := db.Exec("CREATE SCHEMA IF NOT EXISTS " + schema); err != nil {
		return err
	}

	log.WithFields(log.Fields{
		"schema": schema,
	}).Info("Schema created")
	return nil
}

func CreateTable(db *sql.DB, table Table) error {
	table.Logger.Info("Creating table")

	var fields []string
	for _, field := range table.Fields {
		q := field.Name + " " + field.Type
		if field.MaxLen != nil {
			q += fmt.Sprintf("(%d)", *field.MaxLen)
		}
		fields = append(fields, q)
	}

	query := fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s.%s (%s)", table.Schema.Name, table.Name, strings.Join(fields[:], ", "))
	table.Logger.WithFields(log.Fields{
		"query": query,
	}).Debug("Create table query")
	if _, err := db.Exec(query); err != nil {
		return err
	}

	table.Logger.Info("Table created")
	return nil
}

func LoadTable(db *sql.DB, adapter DatabaseAdapter, table Table) error {
	ch := adapter.DownloadData(table)

	columns := make([]string, len(table.Fields))
	for i, field := range table.Fields {
		columns[i] = field.Name
	}

	var txn *sql.Tx
	var stmt *sql.Stmt
	var err error

	count := 0

	for row := range ch {
		if txn == nil {
			txn, err = db.Begin()
			if err != nil {
				return err
			}

			stmt, err = txn.Prepare(pq.CopyInSchema(table.Schema.Name, table.Name, columns...))
			if err != nil {
				return err
			}
		}

		// TODO: fix it
		for idx, field := range table.Fields {
			if adapter.GetType() == ORACLE && field.Type == "integer" && row[idx] != nil {
				val, _ := strconv.ParseInt(row[idx].(string), 10, 64)
				row[idx] = int(val)
			} else if field.Type == "numeric" && row[idx] != nil {
				row[idx], err = strconv.ParseFloat(string(row[idx].([]uint8)), 64)
			} else if IsFixableTextType(field.Type) && row[idx] != nil {
				row[idx] = string(row[idx].([]uint8))
			}
		}

		if _, err := stmt.Exec(row...); err != nil {
			return err
		}

		count++

		if count%100000 == 0 {
			log.Println(count)
		}

		if count%100000 == 0 {
			if _, err := stmt.Exec(); err != nil {
				return err
			}

			if err := stmt.Close(); err != nil {
				return err
			}

			if err := txn.Commit(); err != nil {
				return err
			}

			txn = nil
		}
	}

	if txn != nil {
		if _, err := stmt.Exec(); err != nil {
			return err
		}

		if err := stmt.Close(); err != nil {
			return err
		}

		if err := txn.Commit(); err != nil {
			return err
		}
	}

	return nil
}

func CreatePrimaryKey(db *sql.DB, table Table) error {
	var columns []string
	for _, field := range table.Fields {
		if field.PrimaryKey {
			columns = append(columns, field.Name)
		}
	}

	if len(columns) == 0 {
		return nil
	}

	query := fmt.Sprintf("ALTER TABLE %s.%s ADD PRIMARY KEY (%s)", table.Schema.Name, table.Name, strings.Join(columns, ", "))
	if _, err := db.Exec(query); err != nil {
		return err
	}

	return nil
}
